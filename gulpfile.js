var gulp = require('gulp');
let browserSync = require('browser-sync').create();
var browserify = require('gulp-browserify');

gulp.task('default', ['build', 'serve']);

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: 'public'
        },
    });

    gulp.watch('src/**/*', ['build']);
    gulp.watch('tailwind.js', ['build']);
    gulp.watch('public/*.html').on('change', browserSync.reload);
});

gulp.task('scripts', function() {
  // Single entry point to browserify
  gulp.src('src/main.js')
    .pipe(browserify({
      insertGlobals : true,
      debug : !gulp.env.production
    }))
    .pipe(gulp.dest('public/build/'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('styles', function () {
    let postcss    = require('gulp-postcss');
    let sourcemaps = require('gulp-sourcemaps');
    let important = require('postcss-important-startstop');
    let atImport = require('postcss-import');
    let tailwindcss = require('tailwindcss');
    let autoprefixer = require('autoprefixer');

    return gulp.src('src/main.css')
        .pipe( sourcemaps.init() )
        .pipe( postcss([
            atImport,
            tailwindcss('tailwind.js'),
            important,
            autoprefixer
        ]) )
        .pipe( sourcemaps.write('.') )
        .pipe( gulp.dest('public/build/') )
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('build', ['styles', 'scripts']);