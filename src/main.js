let $ = require('jquery');

require('jquery.mmenu');

$(document).ready(function() {
  $("#mmenu").mmenu({
    navbar: {
      title: false
    }
  });

  $("#side-nav").mmenu({
    navbar: {
      title: false
    },
    offCanvas: false
  });


  $("#side-nav-wrapper").css('display', 'block');
  setMMenuHeight();
  $(document).on('click', '#side-nav-wrapper', function () {
    $("html, body").animate({scrollTop: 0}, "slow");
    setTimeout(function() {
      setMMenuHeight();
    }, 300);
  });

  function setMMenuHeight() {
    let totalHeight = 40;

    $.each($('#side-nav-wrapper .mm-opened > .mm-listview li'), function () {
      totalHeight += $(this).height();
    });
    $('#side-nav-wrapper').css('height', totalHeight);
  }
});

$(document).ready(function() {
  let sticky = $('[data-sticky]');
  sticky.attr('data-sticky', 33);
  sticky.wrap('<div class="sticky-placeholder"></div>')
});